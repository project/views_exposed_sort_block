# Views Exposed Sort Block

Provides a block type and views area plugin which renders views display exposed
sorts separately from the view.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_exposed_sort_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_exposed_sort_block).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

**Add block:**
1.  Enable the module.
2.  Go to block layout (`admin/structure/block`).
3.  Add a block of category "`Views exposed sort block`" - Simply click
    "`Place block`" on the block administration page and search for
    "`Views exposed sort block`". You may add as many of these blocks as you
    need.
4.  Select the view & display which holds the exposed sorts.
5.  Place the block into the region where to display the exposed sorts
    and eventually configure display rules / paths.
6.  Disable AJAX in the view you'd like to use (with ajax is untested).
7.  Place block and result view on the same page so that the sort arguments
    can be handled by the result view.

**Add area plugin:**
1.  Enable the module.
2.  Go to your view.
3.  Add the area plugin "`Views exposed sort block`"
