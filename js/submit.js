(function ($, Drupal, once, drupalSettings) {
  /**
   * Auto submit sort by.
   *
   * @type {{attach: Drupal.behaviors.viewsExposedSortBlockSubmit.attach}}
   */
  Drupal.behaviors.viewsExposedSortBlockSubmit = {
    /**
     * Attach Views Exposed sort block submit behavior.
     *
     * @param context
     * @param settings
     */
    attach(context, settings) {
      const formSelector = drupalSettings.views_exposed_sort_block.form;
      const triggerFields =
        drupalSettings.views_exposed_sort_block.trigger_fields;

      $(once('views-exposed-sort-by', formSelector, context)).each(function () {
        Drupal.behaviors.viewsExposedSortBlockSubmit.bindUiEvents(
          formSelector,
          triggerFields,
          context,
        );
      });
    },

    /**
     * Bind UI Events.
     *
     * @param formSelector
     * @param triggerFields
     * @param context
     */
    bindUiEvents(formSelector, triggerFields, context) {
      for (let i = 0; i < triggerFields.length; i++) {
        const triggerField = `${formSelector} [data-drupal-selector="${triggerFields[i]}"]`;
        $(triggerField).on('change', function () {
          $(this.form).trigger('submit');
        });
      }
    },
  };
})(jQuery, Drupal, once, drupalSettings);
