<?php

namespace Drupal\views_exposed_sort_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Views;
use Drupal\views_exposed_sort_block\ViewsExposedSortInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a separate views exposed sort block.
 *
 * @Block(
 *   id = "views_exposed_sort_block",
 *   admin_label = @Translation("Views Exposed Sort Block")
 * )
 */
class ViewsExposedSortBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The views exposed sort service.
   *
   * @var \Drupal\views_exposed_sort_block\ViewsExposedSortInterface
   */
  protected $viewsExposedSort;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('views_exposed_sort')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ViewsExposedSortInterface $views_exposed_sort) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger_factory->get('views_exposed_sort_block');
    $this->viewsExposedSort = $views_exposed_sort;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_display' => NULL,
      'show_submit' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['view_display'] = [
      '#type' => 'select',
      '#options' => Views::getViewsAsOptions(FALSE, 'enabled'),
      '#title' => $this->t('View & Display'),
      '#default_value' => $this->configuration['view_display'],
      '#required' => TRUE,
    ];
    $form['show_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show submit'),
      '#default_value' => $this->configuration['show_submit'] ?? 1,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view_display'] = $form_state->getValue('view_display');
    $this->configuration['show_submit'] = $form_state->getValue('show_submit');
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $view_display = $form_state->getValue('view_display');
    if (!empty($view_display)) {
      [$view_id, $display_id] = explode(':', $view_display);
      if (empty($view_id) || empty($display_id)) {
        $form_state->setErrorByName('view_display', $this->t('View or display could not be determined correctly from the selected value.'));
      }
      else {
        // Check if the view exists:
        $view = Views::getView($view_id);
        if (empty($view)) {
          $form_state->setErrorByName('view_display', $this->t('View "%view_id" or its given display: "%display_id" doesn\'t exist. Please check the block configuration.', [
            '%view_id' => $view_id,
            '%display_id' => $display_id,
          ]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $view_display = $this->configuration['view_display'];
    if (empty($view_display)) {
      return [];
    }

    [$view_id, $display_id] = explode(':', $view_display);
    if (empty($view_id) || empty($display_id)) {
      return [];
    }

    $view = Views::getView($view_id);
    if (!empty($view)) {
      $view->setDisplay($display_id);

      return $this->viewsExposedSort->buildExposedSortForm($view, $this->configuration);
    }
    else {
      $error = $this->t('View "%view_id" or its given display: "%display_id" doesn\'t exist. Please check the block configuration.', [
        '%view_id' => $view_id,
        '%display_id' => $display_id,
      ]);
      $this->logger->error($error);

      return [
        '#type' => 'inline_template',
        '#template' => '{{ error }}',
        '#context' => [
          'error' => $error,
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Prevent the block from cached else the selected options will be cached.
    return 0;
  }

}
