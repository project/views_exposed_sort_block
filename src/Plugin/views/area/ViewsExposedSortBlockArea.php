<?php

namespace Drupal\views_exposed_sort_block\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\views_exposed_sort_block\ViewsExposedSortInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Area plugin to display the exposed sort options.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_exposed_sort_block")
 */
class ViewsExposedSortBlockArea extends AreaPluginBase {

  /**
   * The views exposed sort service.
   *
   * @var \Drupal\views_exposed_sort_block\ViewsExposedSortInterface
   */
  protected $viewsExposedSort;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('views_exposed_sort')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewsExposedSortInterface $views_exposed_sort) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->viewsExposedSort = $views_exposed_sort;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['show_submit'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['show_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show submit button'),
      '#default_value' => $this->options['show_submit'] ?? 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $view = $this->view;
    if (empty($view)) {
      return [];
    }

    return $this->viewsExposedSort->buildExposedSortForm($view, $this->options);
  }

}
