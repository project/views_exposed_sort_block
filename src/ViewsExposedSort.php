<?php

namespace Drupal\views_exposed_sort_block;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\views\ViewExecutable;

/**
 * Provides a Views exposed Sort service.
 */
class ViewsExposedSort implements ViewsExposedSortInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs an Exposed Sort object.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(FormBuilderInterface $form_builder, LoggerChannelFactoryInterface $logger_factory, EntityTypeManager $entityTypeManager) {
    $this->formBuilder = $form_builder;
    $this->logger = $logger_factory->get('views_exposed_sort_block');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedSortForm(ViewExecutable $view, array $configuration = []): array {

    $view->initHandlers();
    $form_state = (new FormState())
      ->setStorage([
        'view' => $view,
        'display' => &$view->display_handler->display,
        'rerender' => TRUE,
      ])
      ->setMethod('get')
      ->setAlwaysProcess()
      ->disableRedirect();
    $form_state->set('rerender', NULL);
    $form = $this->formBuilder
      ->buildForm('\Drupal\views\Form\ViewsExposedForm', $form_state);

    // Override form action URL in order to allow to place the exposed form
    // block on a different page as the view results.
    if ($view->display_handler->getOption('link_display') == 'custom_url' && !empty($view->display_handler->getOption('link_url'))) {
      $form['#action'] = $view->display_handler->getOption('link_url');
    }

    // Maintain the facets_pretty_path query params.
    $query_plugin = $view->getQuery();
    if ($query_plugin instanceof SearchApiQuery) {
      // Retrieve the facet source.
      $query = $query_plugin->getSearchApiQuery();
      $display_id = $query->getSearchId(FALSE);
      $facet_source_id = str_replace(':', '__', 'search_api:' . $display_id);
      $facet_source = $this->entityTypeManager
        ->getStorage('facets_facet_source')
        ->load($facet_source_id);

      if ($facet_source && $facet_source->getUrlProcessorName() == 'facets_pretty_paths') {
        $form['#action'] = NULL;
      }
    }

    // Do not display exposed filters.
    foreach ($view->display_handler->getHandlers('filter') as $filter_handler) {
      if ($filter_handler->isExposed()) {
        $options = $filter_handler->options;
        $identifier = $options['expose']['identifier'];
        $original_value = $form[$identifier]['#value'];
        $form[$identifier] = [
          '#type' => 'hidden',
          '#name' => $identifier,
          '#value' => $original_value,
        ];
      }
    }

    // Check if we need to display the submit button.
    if (!$configuration['show_submit']) {
      unset($form['actions']['submit']);

      $trigger_fields = [
        'edit-sort-by',
        'edit-sort-order',
      ];

      $form['#attached']['drupalSettings']['views_exposed_sort_block'] = [
        'form' => '#' . $form['#id'],
        'trigger_fields' => $trigger_fields,
      ];
      $form['#attached']['library'][] = 'views_exposed_sort_block/submit';
    }

    return $form;
  }

}
