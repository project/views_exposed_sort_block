<?php

namespace Drupal\views_exposed_sort_block;

use Drupal\views\ViewExecutable;

/**
 * Provides an interface for the Exposed Sort.
 */
interface ViewsExposedSortInterface {

  /**
   * Build the exposed sort form.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param array $configuration
   *   The configuration.
   *
   * @return array
   *   The form.
   */
  public function buildExposedSortForm(ViewExecutable $view, array $configuration = []): array;

}
