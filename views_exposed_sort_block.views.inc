<?php

/**
 * @file
 * Provide views data for views_exposed_sort_block.
 */

/**
 * Implements hook_views_data().
 */
function views_exposed_sort_block_views_data() {

  $data['views']['views_exposed_sort_block'] = [
    'title' => t('Views exposed sort block'),
    'help' => t('Provides a block type which renders views display exposed sorts separately from the view.'),
    'area' => [
      'id' => 'views_exposed_sort_block',
    ],
  ];

  return $data;
}
